FROM node:13.12.0-alpine

# set working directory
WORKDIR /frontend

# add `/app/node_modules/.bin` to $PATH
ENV PATH /frontend/node_modules/.bin:$PATH

# install app dependencies
COPY . /frontend
RUN npm install --silent
RUN npm install react-scripts@3.4.3 -g --silent

ARG DEFAULT_PORT=3000
ENV NODE_PORT=${DEFAULT_PORT}



# start app
CMD ["npm", "start","0.0.0.0:${NODE_PORT}"]