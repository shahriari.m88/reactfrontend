import React, { Fragment, useState, useEffect, useContext } from "react";
import { DropdownButton, Dropdown, MenuItem } from "react-bootstrap"; // npm install react-bootstrap
import { API } from "../API/api-service";
import { useCookies } from "react-cookie";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

function CarFeatures(event) {
  const [cylinders, setCylinders] = useState("");
  const [displacement, setDisplacement] = useState("");
  const [horsepower, setHorsepower] = useState("");
  const [weight, setWeight] = useState("");
  const [acceleration, setAcceleration] = useState("");
  const [modelYear, setModelYear] = useState("");
  const [region, setRegion] = useState("select region");

  const submitClicked = (event) => {
    console.log("Ï am coming from submit Clicked...!");
    console.log("These are the application tags!");

    event.preventDefault();

    API.apiCarFeatures(
      {
        cylinders,
        displacement,
        horsepower,
        weight,
        acceleration,
        modelYear,
        region,
      },

      localStorage.getItem("app-token")
    )
      .then((resp) => resp.json())
      .then((resp) => {
        console.log("working");
        console.log(resp.result.predictions);
      })
      .catch((error) => console.log(error));
  };

  return (
    <React.Fragment>
      <Form>
        <div class="w-50 ">
          <Form.Group>
            <Form.Label>cylinders</Form.Label>
            <Form.Control
              placeholder="Cylinders"
              defaultValue={cylinders}
              onChange={(evt) => setCylinders(evt.target.value)}
            />

            <Form.Label>Displacement</Form.Label>
            <Form.Control
              placeholder="Displacement"
              defaultValue={displacement}
              onChange={(evt) => setDisplacement(evt.target.value)}
            />

            <Form.Label>Horse Power</Form.Label>
            <Form.Control
              placeholder="Horse power"
              defaultValue={horsepower}
              onChange={(evt) => setHorsepower(evt.target.value)}
            />

            <Form.Label>weight</Form.Label>
            <Form.Control
              placeholder="Weight"
              defaultValue={weight}
              onChange={(evt) => setWeight(evt.target.value)}
            />

            <Form.Label>acceleration</Form.Label>
            <Form.Control
              placeholder="Acceleration"
              defaultValue={acceleration}
              onChange={(evt) => setAcceleration(evt.target.value)}
            />

            <Form.Label>Model Year</Form.Label>
            <Form.Control
              placeholder="Model Year"
              defaultValue={modelYear}
              onChange={(evt) => setModelYear(evt.target.value)}
            />

            <Form.Control
              className="regionSelector"
              as="select"
              defaultValue={region}
              custom
              onChange={(evt) => setRegion(evt.target.value)}
            >
              <option>select a region</option>
              <option>europe</option>
              <option>usa</option>
              <option>japan</option>
            </Form.Control>
          </Form.Group>
        </div>
      </Form>

      <div>
        <Button
          variant="btn btn-success"
          size="lg"
          active
          onClick={submitClicked}
        >
          Submit
        </Button>
      </div>
    </React.Fragment>
  );
}

export default CarFeatures;
