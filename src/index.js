import React, { useState, createContext, useEffect } from "react";
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { CookiesProvider, useCookies } from "react-cookie";
import { Route, BrowserRouter } from "react-router-dom";

import CarFeatures from "./ML/CarFeatures"


function Router() {
  const [systemMode, setSystemMode] = useState("ApplicationMode"); // The initial state of the site

  const [token, setToken] = useCookies(["applicant-token"]);
const routing = (
  <React.StrictMode>
    <CookiesProvider>
      <BrowserRouter>
        <Route exact path="/api/ml/mpg" component={CarFeatures} />
 
      </BrowserRouter>
    </CookiesProvider>
  </React.StrictMode>
);
return routing;
}

ReactDOM.render(<Router />, document.getElementById("root"));
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
